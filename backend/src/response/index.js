
const success = data => {
  return {
    status: 'ok',
    data: data
  };
}

const fail = message => {
  return {
    status: 'error',
    data: {
      message: message
    }
  };
}

module.exports = {
  success,
  fail
};
