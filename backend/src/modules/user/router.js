const express = require('express');
const router = express.Router();
const handler = require('./handler');
const verify_token = require('../../middleware/token.middleware.js')

router.post('/sign_in', (req, res, next) => {
  handler.sign_in(req.body)
  .then(val => res.json(val))
  .catch(err => next(err))
});

router.get('/user/info', verify_token, (req, res, next) => {
  handler.get_info(req.payload.id)
  .then(val => res.json(val))
  .catch(err => next(err))
})

router.post('/user/info/update', verify_token, (req, res, next) => {
  handler.update_info(req.body)
  .then(val => res.json(val))
  .catch(err => next(err))
})

module.exports = router;
