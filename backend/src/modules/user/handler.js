const Model = require('./model');
const SHA256 = require("crypto-js/sha256");
const { success } = require('../../response');
const jwt = require("../../jwt")


const sign_in = async (params) => {
  try {
    let { rows = [] } = await Model.get_user(params.email)
    if(rows.length === 0) {
      throw {
        status: 200,
        message: "Email is wrong!"
      }
    } 
        
    if(SHA256(params.password).toString() !== rows[0].password){
      throw {
        status: 200,
        message: "Password is wrong!"
      }
    }

    if(rows[0].is_deleted){
      throw {
        status: 200,
        message: "User is deleted!"
      }
    }

    delete rows[0].password
    
    return success({ token: jwt.encode(rows[0]), user: rows[0]})
  } catch (err) {
    throw err
  }
}

const get_info = async (id) => {
  try {
    let user = await Model.get_user_by_id(id)
    const { rows } = user
    delete rows[0].password

    return success({ user: rows[0] })
  } catch (err) {
    throw err
  }
}

const update_info = async (params) => {
  try {
    await Model.update_user(params)
    let user = await Model.get_user_by_id(params.id)

    const { rows } = user
    delete rows[0].password

    return success({ user: rows[0] })
  } catch (err) {
    throw err
  }
}

module.exports = {
  sign_in,
  get_info,
  update_info
};
