const { load, save } = require('../../db')
const moment = require('moment')

const get_user = (email) => {
  return load(`select * from users where email = '${email}'`)
}

const update_user = (user) => {
  const birthday = moment(user.birthday).format('YYYY-MM-DD HH:mm:ss')
  let query = `UPDATE users
  SET name ='${user.name}', gender = '${user.gender}', cmnd = '${user.cmnd}', birthday = '${birthday}', high_level = '${user.high_level}', specialized = '${user.specialized}'
  where id = ${user.id}
  `
  return save(query)
}

const get_user_by_id = (id) => {
  return load(`select * from users where id = ${id}`)
}

module.exports = {
  get_user,
  update_user,
  get_user_by_id
};
