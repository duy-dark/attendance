const { load, save } = require('../../db')
const moment = require('moment')

const query_teacher_calendar = (user, params) => {
  const start_date = moment(params.start_date).format('YYYY-MM-DD HH:mm:ss')
  const end_date = moment(params.end_date).format('YYYY-MM-DD HH:mm:ss')
  let query = `
    select c.id, c.class_id, s.code, s.name, c.date_learn, c.start_learn, c.end_learn, r.name as room_name,
    (
      select json_agg(t)
      from (
        select distinct cq.id, cq.time_beginner as time
        from collection_questions as cq
        where c.id = cq.calendar_id and cq.is_deleted = ${false}
      ) as t
    ) as questions
    from calendar as c, subjects as s, rooms as r
    where c.subject_id = s.id and c.room_id = r.id  and c.teacher_id = ${user.id} and c.date_learn >= '${start_date}' and  c.date_learn <= '${end_date}'
    ORDER BY c.id ASC 
  `
  return load(query)
}

const query_student_calendar = (user, params) => {
  const start_date = moment(params.start_date).format('YYYY-MM-DD HH:mm:ss')
  const end_date = moment(params.end_date).format('YYYY-MM-DD HH:mm:ss')
  let query = `
    select c.calendar_id as id, c1.class_id, s.code, s.name, c.date_learn, c.start_learn, c.end_learn, r.name as room_name,
    (
      select json_agg(t)
      from (
        select distinct cq.id, cq.time_beginner as time, a.status
        from collection_questions as cq
        left join attendance as a on cq.id = a.collection_question_id and a.student_id = c.student_id and a.is_deleted = false
        where c.calendar_id = cq.calendar_id and cq.is_deleted = false
      ) as t
    ) as questions
    from calendar_students as c, subjects as s, rooms as r, calendar as c1
    where c.subject_id = s.id and c.calendar_id = c1.id and c.room_id = r.id  and c.student_id = ${user.id} and c.date_learn >= '${start_date}' and  c.date_learn <= '${end_date}'
    ORDER BY c.id ASC 
  `
  return load(query)
}

const insert_question = (collection_id, questions) => {
  const now = moment().format('YYYY-MM-DD HH:mm:ss')

  let query = `insert into questions (collection_question_id, question, answer, inserted_at, updated_at, is_deleted) values`
  questions.forEach((item, index) => {
    query += `(${collection_id}, '${item.question}', '${item.answer}', '${now}', null, ${false})`
    if (index !== questions.length - 1) query += ','
  })
  query += ' RETURNING *'
  return save(query)
}

const insert_collection_question = (collection) => {
  const now = moment().format('YYYY-MM-DD HH:mm:ss')
  const time_beginner = moment(collection.time_beginner).format('YYYY-MM-DD HH:mm:ss')
  let query = `insert into collection_questions (calendar_id, time_beginner, inserted_at, updated_at, is_deleted) values
  (${collection.calendar_id}, '${time_beginner}', '${now}', null, ${false}) RETURNING *
  `
  return save(query)
}

const select_collection_question = (id) => {
  let query = `
    select q.id, q.collection_question_id, q.question, q.answer, cq.time_beginner
    from collection_questions as cq, questions as q
    where cq.id = q.collection_question_id and cq.id = ${id} and cq.is_deleted = ${false}
  `
  return load(query)
}

const update_collection_question = (collection) => {
  const now = moment().format('YYYY-MM-DD HH:mm:ss')
  const time_beginner = moment(collection.time_beginner).format('YYYY-MM-DD HH:mm:ss')
  let query = `
    UPDATE collection_questions SET time_beginner = '${time_beginner}', updated_at = '${now}' WHERE id = ${collection.id}
  `
  return save(query)
}

const update_questions = (questions) => {
  const now = moment().format('YYYY-MM-DD HH:mm:ss')
  let query = ``
  questions.forEach(item => {
    query += `UPDATE questions SET question = '${item.question}', answer = '${item.answer}', updated_at = '${now}' WHERE id = ${item.id};`
  })
  return save(query)
}

const delete_questions = (id) => {
  const now = moment().format('YYYY-MM-DD HH:mm:ss')
  let query = `
    UPDATE collection_questions SET is_deleted = ${true}, updated_at = '${now}' where id = ${id};
    UPDATE questions SET is_deleted = ${true}, updated_at = '${now}' WHERE collection_question_id = ${id};
  `
  return save(query)
}

const save_attendance = (body) => {
  const now = moment().format('YYYY-MM-DD HH:mm:ss')
  let query = `insert into attendance (calendar_id, student_id, collection_question_id, status, inserted_at, updated_at, is_deleted) values
  (${body.calendar_id}, ${body.student_id}, ${body.collection_question_id}, ${body.status}, '${now}', null, ${false}) RETURNING *
  `
  return save(query)
}

const get_attendance_by_student = (body) => {
  let query = `
    select *
    from attendance
    where is_deleted = false and calendar_id = ${body.calendar_id} and student_id = ${body.student_id} and collection_question_id = ${body.collection_question_id}
  `
  return load(query)
}

const get_class_teacher = (teacher_id) => {
  let query = `
    select c.id, c.code, s.name 
    from class as c, subjects as s
    where c.subject_id = s.id and teacher_id = ${teacher_id}
  `
  return load(query)
}

const get_class_student = (id) => {
  let query = `
    select c.student_id, s.mssv as student_code
    from class_student as c, students as s
    where c.class_id = ${id}
  `
  return load(query)
}

const get_user_by_code = (mssv) => {
  let query = `
    select user_id
    from students
    where mssv = '${mssv}'
  `
  return load(query)
}

const get_list_attendance = (id) => {
  let query = `
    select u.id as student_id, s.mssv, u.name as student_name, u.birthday, u.cmnd, u.high_level, cl.code as class_code, sb.name as subject_name, c.start_learn, c.end_learn, a.collection_question_id, a.status
    from attendance as a, users as u, students as s, calendar as c, class as cl, subjects as sb
    where a.student_id = u.id and u.id = s.user_id and a.calendar_id = c.id and c.class_id = cl.id and c.subject_id = sb.id and a.calendar_id = ${id} and u.is_deleted = ${false} and a.is_deleted = ${false}
    ORDER BY u.id ASC, a.collection_question_id ASC
  `
  return load(query)
}

const get_collection_question_id = (calendar_id) => {
  let query = `
    select json_agg(id) as ids
    from collection_questions
    where calendar_id = ${calendar_id}
  `
  return load(query)
}

const save_attendance_more = (body) => {
  const { collection_question_ids = [] } = body
  const now = moment().format('YYYY-MM-DD HH:mm:ss')
  let query = `insert into attendance (calendar_id, student_id, collection_question_id, status, inserted_at, updated_at, is_deleted) values `

  collection_question_ids.forEach((item, index) => {
    query += `(${body.calendar_id}, ${body.student_id}, ${item}, ${body.status}, '${now}', null, ${false})`
    if (index !== collection_question_ids.length - 1) query += ','
  })
  return save(query + 'RETURNING *')
}

module.exports = {
  get_collection_question_id,
  save_attendance_more,
  get_list_attendance,
  get_user_by_code,
  get_class_teacher,
  get_attendance_by_student,
  query_teacher_calendar,
  query_student_calendar,
  insert_collection_question,
  insert_question,
  select_collection_question,
  update_collection_question,
  update_questions,
  delete_questions,
  save_attendance,
  get_class_student
};
