const Model = require('./model');
const { success } = require('../../response');
const moment = require('moment')
const { groupBy } = require('lodash')

const get_calendar = async (user, params) => {
  try {
    let calendars
    if (user.permission === 2) {
      calendars = await Model.query_teacher_calendar(user, params)
    }
    if (user.permission === 1) {
      calendars = await Model.query_student_calendar(user, params)
    }

    return success({
      calendars: calendars.rows
    })
  } catch (err) {
    throw err
  }
}

const on_attendance = async (user, body) => {
  try {
    if (user.permission === 2) {
      let { rows: c_students } = await Model.get_class_student(body.class_id)
      if (c_students.filter(item => item.student_code === body.mssv).length === 0) throw {
        status: 200,
        message: `You have no this class!`
      }
      let { rows: collection_questions } = await Model.get_collection_question_id(body.calendar_id)
      let { rows: students } = await Model.get_user_by_code(body.mssv)
      await Model.save_attendance_more({
        calendar_id: body.calendar_id,
        student_id: students[0].user_id,
        collection_question_ids: collection_questions[0].ids,
        status: true
      })
    }

    if (user.permission === 1) {
      attendanced =  await Model.get_attendance_by_student({
        calendar_id: body.calendar_id,
        student_id: user.id,
        collection_question_id: body.collection_question_id
      })

      if (attendanced.rows.length > 0) throw {
        status: 200,
        message: "You attendanced"
      }
      
      collection_question = await Model.select_collection_question(body.collection_question_id)
      let { rows: c_questions } = collection_question
      if (c_questions.length === 0) throw {
        status: 200,
        message: "No questions!"
      }
      let arr = body.answers.filter(item => c_questions.find(question => question.id === item.id && item.answer !== question.answer))

      let { rows: c_students } = await Model.get_class_student(body.class_id)
      if (c_students.filter(item => item.student_id === user.id).length === 0) throw {
        status: 200,
        message: `You have no this class!`
      }
      console.log(arr)
      await Model.save_attendance({
        calendar_id: body.calendar_id,
        student_id: user.id,
        collection_question_id: body.collection_question_id,
        status: arr.length === 0
      })
    }

    return success({
      message: "Attendance successfull"
    })
  } catch (err) {
    throw err
  }
}

const get_student_calendar = async (body) => {
  try {
    let list_attendance = await Model.get_list_attendance(body.calendar_id)
    let { rows: questions } = await Model.get_collection_question_id(body.calendar_id)
    let arr = Object.values(groupBy(list_attendance.rows, 'student_id')).map(item => {
      let obj = {...item[0]}
      let collection_questions = item.map(val => val.collection_question_id)
      let arr_tmp = item.map(val => ({
        id: val.collection_question_id,
        status: val.status
      }))
      delete obj.collection_question_id
      delete obj.status
      return {
        ...obj,
        collection_questions: questions[0].ids.map(val => {
          if (collection_questions.includes(val)) {
            return arr_tmp.find(k => k.id === val)
          }
          return {
            id: val,
            status: null
          }
        })
      }
    })
    return success({
      attendances: arr
    })
  } catch (err) {
    throw err
  }
}

const get_class = async (user) => {
  try {
    let class_teacher = []
    if (user.permission === 2) {
      class_teacher = await Model.get_class_teacher(user.id)
    }

    return success({
      class: class_teacher.rows
    })
  } catch (err) {
    throw err
  }
}

const get_collection_question = async (id) => {
  try {
    let collection = await Model.select_collection_question(id)
    return success({
      questions: collection.rows
    })
  } catch (err) {
    throw err
  }
}

const create_question = async (params) => {
  try {
    let param_collection = {
      calendar_id: params.calendar_id,
      time_beginner: params.time_beginner
    }
    let { rows = [] } = await Model.insert_collection_question(param_collection)
    let questions = await Model.insert_question(rows[0].id, params.questions)

    return success({
      ...param_collection,
      id: rows[0].id,
      questions: questions.rows
    })
  } catch (err) {
    throw err
  }
}


const update_question = async (params) => {
  try {
    let param_collection = {
      id: params.collection_question_id,
      time_beginner: params.time_beginner
    }
    let p1 = Model.update_collection_question(param_collection)
    let p2 = Model.update_questions(params.questions)
    await Promise.all([p1, p2])
    let collection = await Model.select_collection_question(param_collection.id)

    return success({
      ...params,
      questions: collection.rows
    })
  } catch (err) {
    throw err
  }
}

const delete_question = async (params) => {
  try {
    await Model.delete_questions(params.collection_question_id)

    return success(params)
  } catch (err) {
    throw err
  }
}

module.exports = {
  get_calendar,
  on_attendance,
  get_collection_question,
  create_question,
  update_question,
  delete_question,
  get_class,
  get_student_calendar
};
