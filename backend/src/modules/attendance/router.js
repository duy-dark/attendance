const express = require('express');
const router = express.Router();
const handler = require('./handler');

router.get('/calendar', (req, res, next) => {
  handler.get_calendar(req.payload, req.query)
  .then(val => res.json(val))
  .catch(err => next(err))
});

router.get('/class', (req, res, next) => {
  handler.get_class(req.payload)
  .then(val => res.json(val))
  .catch(err => next(err))
});

router.post('/post', (req, res, next) => {
  handler.on_attendance(req.payload, req.body)
  .then(val => res.json(val))
  .catch(err => next(err))
});

router.get('/collection/question/:id', (req, res, next) => {
  handler.get_collection_question(req.params.id)
  .then(val => res.json(val))
  .catch(err => next(err))
});

router.post('/collection/question/create', (req, res, next) => {
  handler.create_question(req.body)
  .then(val => res.json(val))
  .catch(err => next(err))
});

router.post('/collection/question/update', (req, res, next) => {
  handler.update_question(req.body)
  .then(val => res.json(val))
  .catch(err => next(err))
});

router.post('/collection/question/delete', (req, res, next) => {
  handler.delete_question(req.body)
  .then(val => res.json(val))
  .catch(err => next(err))
});

router.get('/student/get', (req, res, next) => {
  handler.get_student_calendar(req.query)
  .then(val => res.json(val))
  .catch(err => next(err))
});



module.exports = router;
