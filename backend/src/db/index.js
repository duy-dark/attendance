const { Client } = require("pg");
const { DBDatabase, DBUser, DBPassword, DBHost, DBPost } = require("../config")

const options = {
  user: DBUser,
  host: DBHost,
  database: DBDatabase,
  password: DBPassword,
  port: DBPost
}

const connection = () => new Client(options)

const load = sql => {
  return new Promise((resolve, reject) => {
    const cn = connection();
    cn.connect();
    cn.query(sql, (err, rows) => {
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
      cn.end();
    });
  });
}

const save = sql => {
  return new Promise((resolve, reject) => {
    var cn = connection();
    cn.connect();
    cn.query(sql, function(error, value) {
      if (error) {
        reject(error);
      } else {
        resolve(value);
      }
      cn.end();
    });
  });
}

module.exports = {
  load, save
}