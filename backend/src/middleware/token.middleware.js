// const jwt = require('jsonwebtoken')
const { fail } = require('../response')
const jwt = require("../jwt")


module.exports = async (req, res, next) => {
  let token = req.body.token || req.query.token || req.headers.authorization || req.headers.Authorization
  if (token) {
    if (token.startsWith('Bearer ')) {
      token = token.slice(7, token.length).trimLeft();
    }
    let payload = await jwt.decode(token)
    req.payload = payload.user
    next()
  } else {
    res.status(200).json(fail('invalid token!'))
  }
}