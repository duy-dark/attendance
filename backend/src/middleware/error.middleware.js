const { fail } = require('../response')

module.exports = (err, req, res, next) => {
  const { status = 404, message } = err
  return res.status(status).json(fail(message))
}