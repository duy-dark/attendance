const express = require('express'),
      cors = require('cors'),
      bodyParser = require('body-parser')
const { load } = require('./db')
// start import you define
const { port } = require('./config')
const error_handler = require('./middleware/error.middleware.js')
const verify_token = require('./middleware/token.middleware.js')
const morgan = require('morgan')
// end import you define

const app = express()
app.use(cors());
app.use(bodyParser.json());
app.use(morgan('combined'));

// start code api
app.get('/', async (req, res) => {
  res.json('hello world of Duy')
})

app.use('/api', require('./modules/user'))
app.use('/api/attendance', verify_token, require('./modules/attendance'))


// end code api
//start handler errors
app.use(error_handler);

// app.use((req, res) => {
//   res.status(404).json(resFail(404, 'api not found'));
// });

//end handler errors
//start serve

const startSever = async () => {
  app.listen(port, async () => {
    console.log(`QLBH API is running on port ${port}`);
  });
};
startSever();