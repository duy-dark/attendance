const { privateKey, publicKey } = require('../config')


const jwt = require('jsonwebtoken')
const encode = (data) => {
  let payload = {
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30),
    user: data,
    info: 'token is valid'
  }
  return jwt.sign(payload, privateKey, { algorithm: 'RS256' });
}

const decode = (token) => {
  return jwt.verify(token, publicKey);

}

module.exports = { encode, decode }